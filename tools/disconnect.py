#!/usr/bin/env python3

# Written by Kim Brose <kim.brose@rwth-aachen.de> for IENT of RWTH Aachen
# with inspiration by https://github.com/igomezal/bluetoothctl_helper

# note: EV3 uses Bluetooth SPP (Serial Port Profile) to connect to a virtual serial port.
# Your system must have this profile enabled. See the lmcd doc for more info.

from connect import *


def print_list(binds, pairings):
    i = 1
    l = pairings
    for b in binds:
        print(f'{i}) /dev/{b.device:8} bound to {b.MAC} (using Bluetooth channel {b.channel})')
        for p in pairings:
            if b.MAC == p.MAC:
                l.remove(p)
        i += 1

    for p in l:
        print(f'{i}) paired, but unbound device {p.name} ({p.MAC})')
        i += 1

    return l


def find_existing_pairings():
    args = ['bluetoothctl']
    with sp.Popen(args, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE) as btctl:
        write(btctl, 'paired-devices')
        sleep(1)
        write(btctl, 'exit')
        try:
            output, _ = btctl.communicate(timeout=3)
        except sp.TimeoutExpired:
            btctl.kill()
            output, _ = btctl.communicate()
        output = output.decode("utf-8").strip()

    output = output.split(sep='\n')
    unspecific = re.compile(r'^Device\s+([A-Z0-9:]{17})\s+(.*)$')
    f = unspecific
    l = list()
    for brick in output:
        if f.search(brick):
            brick = f.split(brick)
            brick = list(filter(None, brick)) # remove empty strings
            brick = BTDevice(MAC=brick[0], name=brick[1])
            l.append(brick)

    return l


def print_dc_menu():
    binds = find_existing_binds()
    pairings = find_existing_pairings()

    if len(binds) <= 0 and len(pairings) <= 0:
        print('No devices are connected.')
        exit(0)

    pairings = print_list(binds, pairings)

    selection = None
    while not (selection in range(1, len(binds) + len(pairings) + 1)):
        selection = input('Enter number to select: ')
        try:
            selection = int(selection)
        except:
            selection = None

    if selection in range(1, len(binds) + 1):
        b = binds[selection - 1]
    else:
        b = pairings[selection - len(binds) - 1]

    try:
        print(f'\nClose matlab or make sure you disconnect /dev/{b.device} inside matlab,')
        print('for example by using "b.disconnect()".\n')
        input('Then press Enter to continue.')
    except:
        print()

    try:
        args = f'sudo rfcomm release {b.device}'.split()
        rfcomm = sp.run(args, capture_output=True)
    except:
        print()
    # debug
    # print(rfcomm)

    args = ['bluetoothctl']
    with sp.Popen(args, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE) as btctl:
        write(btctl, f'remove {b.MAC}')
        sleep(1)
        write(btctl, 'exit')
        try:
            output, _ = btctl.communicate(timeout=3)
        except sp.TimeoutExpired:
            btctl.kill()
            output, _ = btctl.communicate()
        output = output.decode("utf-8").strip()
        output = output.split(sep='\n')
    # debug
    # print(output)

    print('Done.\n')
    sleep(1)
    print('Remaining connected:', end='')

    binds = find_existing_binds()
    pairings = find_existing_pairings()

    if len(binds) <= 0:
        print(' None')
    else:
        print()
        print_list(binds, pairings)


def main():
    print_dc_menu()


if __name__ == "__main__":
    main()
