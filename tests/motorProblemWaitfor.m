function motorProblemWaitfor( )
% This function will provoke an error in the destructor (at line 14).

brick = EV3();
%brick.connect('usb');
brick.connect('bt', 'serPort', '/dev/rfcomm0');
motor = brick.motorA;

motor.setProperties('power',30,'limitMode', 'Tacho', 'limitValue', 1000, 'brakeMode', 'Coast')
motor.resetTachoCount();
motor.start;
% Problem
motor.waitFor(); %this does not account for coasting
% % Solution to the problem
% while motor.currentSpeed > 0
% end


brick.beep;
brick.disconnect();
end

