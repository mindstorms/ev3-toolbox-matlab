clear all

b = EV3();
b.connect('serPort', '/dev/rfcomm0');
b.sensor3.mode = DeviceMode.Color.Col;
b.sensor4.mode = DeviceMode.UltraSonic.DistCM;
tic;
t = 0;

b.motorA.setProperties('power', 50, 'speedRegulation', 'on', 'smoothStart', 10, 'limitMode', ...
    'Time', 'limitValue', 3000);
b.motorA.start();

pause(0.5);
while b.motorA.isRunning()
    b.sensor3.value
    b.sensor4.value
    b.motorA.tachoCount
end

b.motorB.power = 50;
b.motorB.limitValue = 4*360;
b.motorB.start();
b.motorB.waitFor();
b.beep();

b.motorA.speedRegulation = false;
pause(1);
b.motorA.syncedStart(b.motorB, 'turnRatio', 200);
b.motorA.waitFor();

for i=1:10
    b.sensor3.value
    b.motorA.tachoCount
    b.sensor3.reset
    b.motorA.tachoCount
end

b.beep
b.beep


b.disconnect
b.delete