function motorProblemStart()
% Call this multiple times, then one of the three movements will fail.

brick = EV3();
brick.connect('usb');
%brick.debug = 2;

motor = brick.motorA;
motor.setProperties('power',30,'limitMode', 'Tacho', 'limitValue', 1000, 'brakeMode', 'Coast')

figure; hold all;
for power=30:20:70
    motor.power = power;
    motor.resetTachoCount();
    % Problem: sometimes, the start command is ignored
    motor.start;
    
    tic
    t = toc;
    tVec = []; tachoVec = [];
    while t < 6
       t = toc;
       tVec = [tVec, t]; %#ok<AGROW>
       tachoVec = [tachoVec, motor.tachoCount]; %#ok<AGROW>
    end
    motor.waitFor();
    plot(tVec,tachoVec); drawnow;
end
brick.disconnect();

end

