function packetsPerSecond(brick)
    MAX_ITERATIONS = 100;
    
    dt = [];
    tic;
    for i=1:MAX_ITERATIONS
        temp = brick.inputReadSI(0, 16, 0);
        dt = [dt, toc];
    end
    
    transmission = diff(dt);
    meanTransmission = mean(transmission);
    
    fprintf('Mean transmission: %.4fs (+- %.4fs)\n', meanTransmission, std(transmission));
    fprintf('-> ~ %d packets per second\n', int32(1/meanTransmission));
end

