%Variablen bitte anpassen
clear all;
brick=EV3;
brick.connect('bt', 'serPort', '/dev/rfcomm1');
motorToTest = brick.motorC;
N = 10;

tic;
time = [];
vPower = [];
vBrakeMode = [];
vTachoCount = [];
vLimitValue = [];

%Schleifendurchlauf
for j = 1 : N
    
    [limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode] = generateRandomMotorParams;
    executeMotorCmd(limitValue, 'Tacho', brakeMode, speedRegulation, abs(power), syncedMode, brick, motorToTest);
    motorToTest
    disp(syncedMode)

    while(motorToTest.isRunning())
    end%while
    pause(3);
    vPower(end+1) = motorToTest.power();
    if (strcmp(motorToTest.brakeMode(), 'Brake'))
        vBrakeMode(end+1) = 1;
    else
        vBrakeMode(end+1) = 0;
    end%if
    vTachoCount(end+1) = motorToTest.tachoCount();
    vLimitValue(end+1) = motorToTest.limitValue();
    motorToTest.resetTachoCount();
end%for

%fprint(vPower, vBrakeMode, vTachoCount, vLimitValue);
brick.disconnect;
clear all;