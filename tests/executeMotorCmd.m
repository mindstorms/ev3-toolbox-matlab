function [] = executeMotorCmd(limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode, ev3, motorToTest)
%executeMotorCmd Assigns the parameters as values for an EV3 object and
%tests a given motor
%   The parameters will be assigned as values for a specific motor object.
%   The motor will be started, in normal mode or synced with another motor
%   of the EV3 object.


motorToTest.limitValue = limitValue;
motorToTest.limitMode = limitMode;
motorToTest.brakeMode = brakeMode;
motorToTest.speedRegulation = speedRegulation;
motorToTest.power = power;

if ((~syncedMode) || (strcmp(motorToTest.type, 'MediumMotor')))
    motorToTest.start();
else
    if strcmp(char(ev3.motorA.type), 'LargeMotor') && (ev3.motorA.limitValue ~= motorToTest.limitValue)
        motorToTest.syncedStart(ev3.motorA)        
    end%if
    if strcmp(char(ev3.motorB.type), 'LargeMotor') && (ev3.motorB.limitValue ~= motorToTest.limitValue)
        motorToTest.syncedStart(ev3.motorB)
     end%if
    if strcmp(char(ev3.motorC.type), 'LargeMotor') && (ev3.motorC.limitValue ~= motorToTest.limitValue)
        motorToTest.syncedStart(ev3.motorC)
    end%if
    if strcmp(char(ev3.motorD.type), 'LargeMotor') && (ev3.motorD.limitValue ~= motorToTestlimitValue)
        motorToTest.syncedStart(ev3.motorD)        
    end%if
end%if
end%function