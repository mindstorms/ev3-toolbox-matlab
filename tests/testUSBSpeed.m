clear all;

b = EV3();
b.connect('bt', 'serPort', '/dev/rfcomm1');
%b = CommunicationInterface('bt', 'serPort', '/dev/rfcomm1');

p = 0;
tic;
while toc < 10
    temp = b.sensor1.value;
    p = p+1;
end

fprintf('Packets per second: %.2f\n', p / 10.0);

clear all;