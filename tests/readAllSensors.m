function [port1, port2, port3, port4] = readAllSensors(varargin)

b = CommunicationInterface(varargin{:});

mode = 0;

cmd = Command();
cmd.addHeaderDirectReply(42,4*4,0);
cmd.opINPUT_READSI(0,SensorPort.Sensor1,0,0,0);
cmd.opINPUT_READSI(0,SensorPort.Sensor2,0,0,4);
cmd.opINPUT_READSI(0,SensorPort.Sensor3,0,0,8);
cmd.opINPUT_READSI(0,SensorPort.Sensor4,0,0,12);
cmd.addLength();
b.send(cmd);
msg = b.receive()';

port1 = typecast(uint8(msg(6:9)),'single');
port2 = typecast(uint8(msg(10:13)),'single');
port3 = typecast(uint8(msg(14:17)),'single');
port4 = typecast(uint8(msg(18:21)),'single');

end

