%Variablen bitte anpassen
clear all;
brick=EV3;
brick.connect('bt', 'serPort', '/dev/rfcomm1');
motorToTest = brick.motorC;
N = 50;

tic;
time = [];
plotTachoCount = [];
plotSpeedRegulation = [];

figure('NumberTitle', 'off', 'Name', 'TachoCount vs SpeedRegulation');

%Schleifendurchlauf
for j = 1 : N
    
    [limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode] = generateRandomMotorParams;
    executeMotorCmd(limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode, brick, motorToTest);
    motorToTest
    disp(syncedMode)

    while (motorToTest.isRunning())
        
        time(end+1) = toc;

        %Power/TachoCount/SpeedReg
        plotTachoCount(end+1) = motorToTest.tachoCount/10;
        plotSpeedRegulation(end+1) = motorToTest.speedRegulation*30;
        plot(time, plotTachoCount, time, plotSpeedRegulation);
        legend('TachoCount', 'SpeedRegulation')
        drawnow;

    end%while
   
    xxx = toc;
    delta = 0;
    
    while (delta <= 3)
        time(end+1) = toc;
        plotTachoCount(end+1) = motorToTest.tachoCount()/10;
        plotSpeedRegulation(end+1) = motorToTest.speedRegulation()*30;
        plot(time, plotTachoCount, time, plotSpeedRegulation);
        legend('TachoCount','SpeedRegulation')
        drawnow;
        delta = toc - xxx;
    end%while
    motorToTest.resetTachoCount();
    pause(1);
end%for

brick.disconnect;
clear all;