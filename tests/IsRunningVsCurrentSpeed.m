%Variablen bitte anpassen
clear all;
brick=EV3;
brick.connect('bt');
motorToTest = brick.motorC;
N = 50;

tic;
time = [];
plotIsRunning = [];
plotCurrentSpeed = [];

%plotPower = [];
figure('NumberTitle', 'off', 'Name', 'IsRunning vs CurrentSpeed');

%Schleifendurchlauf
for j = 1 : N

    [limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode] = generateRandomMotorParams;
    executeMotorCmd(limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode, brick, motorToTest);
    motorToTest
    disp(syncedMode)

    while (motorToTest.isRunning())
        
        time(end+1) = toc;
        
        %IsRunning/CurrentSpeed
        plotIsRunning(end+1) = motorToTest.isRunning()*25;
        plotCurrentSpeed(end+1) = motorToTest.currentSpeed();
        plot(time, plotIsRunning, time, plotCurrentSpeed);
        legend('isRunning', 'currentSpeed');
        drawnow;
        
    end%while
   
    xxx = toc;
    delta = 0;
    while (delta <= 3)
        time(end+1) = toc;
        plotIsRunning(end+1) = motorToTest.isRunning()*25;
        plotCurrentSpeed(end+1) = motorToTest.currentSpeed();
        plot(time, plotIsRunning, time, plotCurrentSpeed);
        legend('isRunning', 'currentSpeed');
        drawnow;
        
        delta = toc - xxx;
    end%while
    
end%for

brick.disconnect;
clear all;
