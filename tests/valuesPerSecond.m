function valuesPerSecond(brick) 
    MAX_ITERATIONS = 100;
    
    dt = [];
    tic;
    for i=1:MAX_ITERATIONS
        temp = brick.motorA.tachoCount;
        dt = [dt, toc];
    end
    
    transmission = diff(dt);
    meanTransmission = mean(transmission);
    
    fprintf('Mean transmission: %.4fs (+- %.4fs)\n', meanTransmission, std(transmission));
    fprintf('-> ~ %d values per second\n', int32(1/meanTransmission));
end