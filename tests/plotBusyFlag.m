function plotBusyFlag(brick, varargin)
    p = inputParser();
    
    powerValid = @(x) (x>=-100 && x <= 100);
    limitValid = @(x) (x>=0);
    
    p.addOptional('power', 50);
    p.addOptional('limitValue', 3*360);
    p.addOptional('speedRegulation', false);
    p.addOptional('smoothStart', 0);
    p.addOptional('smoothStop', 0);
    p.addOptional('brakeMode', 'coast');
    p.addOptional('limitMode', 'Tacho');
    p.addOptional('synced', false);
    
    p.parse(varargin{:});
    synced = p.Results.synced;
    
%     if ~isDeviceValid('EV3', brick) || ~brick.isConnected
%         error('Given object is not a connected EV3-object.'); 
%     end
    
    ma = brick.motorA;
    mb = brick.motorB;
    
    ma.resetTachoCount;
    if synced
        mb.resetTachoCount;
    end
    ma.setProperties('power', p.Results.power, 'brakeMode', p.Results.brakeMode, ...
                     'limitMode', p.Results.limitMode, 'limitValue', p.Results.limitValue, ...
                     'smoothStart', p.Results.smoothStart, ...
                     'smoothStop', p.Results.smoothStop, ...
                     'speedRegulation', p.Results.speedRegulation);
    
    t = [];
    flag = [];
    tacho = [];
    
    stopSent = false;
    stopTime = 0;
    if ~synced
        tic;
        ma.start();
        while(toc < 10)
            flag = [flag, ma.isRunning];
            tacho = [tacho, ma.tachoCount];
            t = [t, toc];
        end
        if ma.limitValue == 0
            ma.stop();
        end
        tacho = [tacho, ma.tachoCount];
        flag = [flag, ma.isRunning];
        t = [t, toc];
    else
        tic;
        ma.syncedStart(mb);
        while(toc < 10)
            flag = [flag, ma.isRunning];
            tacho = [tacho, ma.tachoCount];
            t = [t, toc];
            if ma.limitValue == 0
                if (~stopSent && toc > 8)
                    stopTime = toc;
                    ma.syncedStop();
                    stopSent = true;
                end
            end
        end
    end
    hold all
    plot(t,tacho,t,max(tacho)*int32(flag),'-.or')  
    %plot([stopTime stopTime],[0 1000], '.-m')  

    grid on
end