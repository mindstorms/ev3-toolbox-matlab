function [] = randomMotorTestPlot
%testMotor Random test for motors
%   Starts N times the motor motorToTest of the object ev3

%Variablen bitte anpassen
clear all;
brick=EV3;
brick.connect('bt', 'serPort', '/dev/rfcomm0');
motorToTest = brick.motorC;
TIMEOUT_SEC = 20.0;
N = 10;

% %Initialisiere Plot
% tic;
% plotIsRunning = [];
% plotPower = [];
% plotTachoCount = [];
% plotCurrentSpeed = [];
% time = [];
% figure;
% grid on;
% %legend('isRunning', 'power', 'tachoCount', 'currentSpeed');

%Schleifendurchlauf
for j = 1 : N

    [limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode] = generateRandomMotorParams;
    executeMotorCmd(limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode, brick, motorToTest);
    motorToTest
    disp(syncedMode)
    
    
    
    while motorToTest.isRunning
        
    end%while
    
    
    
%     hTimeout = tic();
%     timeoutHappened = false;
%     while (motorToTest.isRunning())
%         
%         if (toc(hTimeout) > TIMEOUT_SEC)
%             timeoutHappened = true;
%             break;
%         end%if
% 
%         %update plot
%         time = [time, toc];
%         plotIsRunning = 1;
%         plotPower = [plotPower, motorToTest.power];
%         %plotTachoCount = [plotTachoCount, motorToTest.tachoCount/100];
%         %plotCurrentSpeed = [plotCurrentSpeed, motorToTest.currentSpeed*10];
%         plot(time, plotIsRunning, time, plotPower);
%         %legend('isRunning', 'power', 'tachoCount', 'currentSpeed');
%         drawnow;
% 
%     end%while
% 
%    % optional: update plot
% %     time = [time, toc];
% %     plotIsRunning = 0;
% %     plotPower = [plotPower, motorToTest.power];
% %     plotTachoCount = [plotTachoCount, motorToTest.tachoCount/100];
% %     plotCurrentSpeed = [plotCurrentSpeed, motorToTest.currentSpeed*10];
% %     plot(time, plotIsRunning, time, plotCurrentSpeed, time, plotPower, time, plotTachoCount);
% %     drawnow;
% 
%     if timeoutHappened
%         warning('Timeout happened');
%     end%if
%     
%     pause(1.0);
    
end%for

brick.disconnect;
clear all;

end%function
