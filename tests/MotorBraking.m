b = EV3('debug', 2);
b.connect('bt', 'serPort', '/dev/rfcomm0');
m = b.motorA;
sl = b.motorB;

m.tachoLimit = 1000;
m.brakeMode = 'Coast';

counts = [];
power = [60, 65, 70, 75, 80, 85, 90, 95, 100];

for i=1:length(power)
    m.resetTachoCount;
    m.reset;
    m.power = power(i);
    m.syncedStart(sl);
    m.waitFor();
    counts = [counts, m.tachoCount];
    b.beep;
    b.debug = false;
end