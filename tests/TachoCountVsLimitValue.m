%Variablen bitte anpassen
clear all;
brick=EV3;
brick.connect('bt');
motorToTest = brick.motorC;
N = 50;

tic;
time = [];
plotIsRunning = [];
plotTachoCount = [];
plotLimitValue = [];

%plotPower = [];
figure('NumberTitle', 'off', 'Name', 'TachoCount vs LimitValue'); 

%Schleifendurchlauf
for j = 1 : N
    
    [limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode] = generateRandomMotorParams;
    executeMotorCmd(limitValue, 'Tacho', brakeMode, speedRegulation, power, syncedMode, brick, motorToTest);
    motorToTest
    disp(syncedMode)

    while (motorToTest.isRunning())
        
        time(end+1) = toc;

        %LimitValue/TachoCount
        plotTachoCount(end+1) = motorToTest.tachoCount();
        plotLimitValue(end+1) = motorToTest.limitValue();
        plot(time, plotTachoCount, time, plotLimitValue);
        drawnow;

    end%while
   
    xxx = toc;
    delta = 0;
    
    while (delta <= 3)
        time(end+1) = toc;
        plotTachoCount(end+1) = 0;
        plotLimitValue(end+1) = motorToTest.limitValue();
        plot(time, plotTachoCount, time, plotLimitValue);
        drawnow;
        
        delta = toc - xxx;
    end%while
    motorToTest.resetTachoCount();
end%for

brick.disconnect;
clear all;