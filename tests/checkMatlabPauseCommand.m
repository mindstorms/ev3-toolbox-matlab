close all
clear all

N = 5000;
t = zeros(N, 1);
tic 

t = 0;
j = 0;
while(toc < 5)
    j = j + 1;
    t(j) = toc;
    pause(0.010);
end

figure;
plot(t, '.-')
grid on
hold all

figure;
hist(diff(t));
    