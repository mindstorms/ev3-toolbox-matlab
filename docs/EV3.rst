.. automodule:: source

.. |br| raw:: html
   
   <br />
EV3
===

.. autoclass:: EV3
   :members: connect, disconnect, stopAllMotors, beep, playTone, stopTone, tonePlayed, setProperties


