.. automodule:: source

.. |br| raw:: html

   <br />
Sensor
======

.. autoclass:: Sensor
   :members: reset, setProperties
