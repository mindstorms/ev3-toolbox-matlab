.. automodule:: source

.. |br| raw:: html
   
   <br />
btBrickIO
=========

.. autoclass:: btBrickIO
   :members: open, close, read, write, setProperties
