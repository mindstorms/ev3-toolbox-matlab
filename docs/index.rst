.. source documentation master file, created by
   sphinx-quickstart on Fri Nov 25 16:05:22 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================================================
MATLAB Toolbox for controlling Lego Mindstorms EV3
==================================================

Hi there! This is the documentation for the "Lego Mindstorms EV3" MATLAB Toolbox,
developed by RWTH Aachen. For an introduction about this toolbox, installation
guides and examples, take a look at `our repository`_.

.. _our repository: https://git.rwth-aachen.de/mindstorms/ev3-toolbox-matlab/blob/master/readme.md

Contents
========

High-Level documentation

.. toctree::
   :maxdepth: 2

   EV3
   Motor
   Sensor

Low-Level documentation

.. toctree::
   :maxdepth: 3

   hid
   usbBrickIO
   btBrickIO
