.. automodule:: source

.. |br| raw:: html

   <br />
Motor
=====

.. autoclass:: Motor
   :members: start, stop, syncedStart, syncedStop, waitFor, internalReset, resetTachoCount, setBrake, setProperties
