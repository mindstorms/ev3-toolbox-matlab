function isValid = isModeValid(mode, type)
% Returns whether given mode is a valid mode in given type.
    if strcmp(class(mode), 'DeviceMode.Default')
        isValid = true;
        return;
    end
    
    % A mode is valid for a given type if the name of the type (e.g. Color)
    % equals the name of the enumeration corresponding to the mode (e.g. Color in
    % DeviceMode.Color.Ambient)
    isValid = strcmp(char(type), strrep(class(mode), 'DeviceMode.', ''));
end