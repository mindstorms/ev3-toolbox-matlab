classdef Sensor < MaskedHandle      
    % Information given in this section can be used to configure a sensor's measurements.
    % For example the Touch-Sensor is capable of either detecting whether it is being pushed, or count the number of pushes.
    % In order to change it's mode and hence it's return values, an EV3 object has to be created and connected beforehand. Assuming the physical sensor has 
    % been connected to sensor port 1 of the physical brick, the mode change is done as follows:
    % 
    % ::
    %
    %     Example:
    %         //initialization:
    %         brick = EV3()
    %         brick.connect('usb')
    %
    %         //changing mode of sensor:
    %         brick.sensor1.mode = DeviceMode.Touch.Bumps
    % 
    % The available modes to a given sensor are described in the Attributes section.
    % 
    %
    % Notes:
    %     * You don't need to create instances of this class. The EV3-class automatically creates
    %       instances for each sensor port, and you can work with them via the EV3-object. 
    %     * The Sensor-class represents sensor ports, not individual sensors!
    %     * When an input argument of a method is marked as optional, the argument needs to be 
    %       'announced' by a preceding 2nd argument, which is a string containing the name of the argument. 
    %       For example, Motor.setProperties may be given a power-parameter. The syntax would be as
    %       follows: *brickObject.motorA.setProperties('power', 50);*
    %
    % Attributes:
    %    mode (DeviceMode.{Type}): Sensor mode in which the value will be read. By default, mode is set to *DeviceMode.Default.Undefined*. See also :attr:`type`. *[WRITABLE]* |br| Once a physical sensor is connected to the port *and* the physical Brick is connected to the EV3-object, the allowed mode and the default mode for a Sensor-object are the following (depending on the sensor type):
    %
    %             * Touch-Sensor: 
    %                 * DeviceMode.Touch.Pushed *[Default]*
    %                     * Output: 0: not pushed, 1: pushed
    %                 * DeviceMode.Touch.Bumps
    %		          * Output: n: number of times being pushed
    %             * Ultrasonic-Sensor: 
    %                 * DeviceMode.UltraSonic.DistCM *[Default]*
    %		          * Output: distance in cm
    %		          * Note: actively creates ultrasonic sound
    %                 * DeviceMode.UltraSonic.DistIn
    %		          * Output: distance in inches
    %		          * Note: actively creates ultrasonic sound
    %                 * DeviceMode.UltraSonic.Listen
    %		          * Output: distance in cm
    %		          * Note: ONLY listens to other sources (sensors) of ultrasonic sound
    %             * Color-Sensor: 
    %                 * DeviceMode.Color.Reflect *[Default]*
    %		          * Output: value in range 0% to 100% brightness
    %                 * DeviceMode.Color.Ambient
    %		          * Output: value in range 0% to 100% brightness
    %                 * DeviceMode.Color.Col
    %		          * Output: none, black, blue, green. yellow, red, white, brown
    %             * Gyro-Sensor: 
    %                 * DeviceMode.Gyro.Angular *[Default]*
    %		          * Note: value appears to be rising indefinitely, even in resting position
    %                 * DeviceMode.Gyro.Rate
    %		          * Output: rotational speed [degree/s]. Expect small offset in resting position
    %             * Infrared-Sensor:
    %                 * DeviceMode.InfraRed.Prox *[Default]*
    %		          * Note: currently not recognized
    %                 * DeviceMode.InfraRed.Seek
    %                 * DeviceMode.InfraRed.Remote
    %             * NXTColor-Sensor:
    %                 * DeviceMode.NXTColor.Reflect *[Default]*
    %		          * Output: value in range 0% to 100% brightness
    %                 * DeviceMode.NXTColor.Ambient
    %		          * Output: value in range 0% to 100% brightness
    %                 * DeviceMode.NXTColor.Color
    %		          * Output: value representing color: 1-black, 2-blue, 3-green, 4-yellow, 5-red, 6-white, 7-brown
    %                 * DeviceMode.NXTColor.Green
    %		          * Output: value in range 0% to 100% of green reflectivity
    %                 * DeviceMode.NXTColor.Blue
    %		          * Output: value in range 0% to 100% of blue reflectivity
    %                 * DeviceMode.NXTColor.Raw
    %		          * Note: obsolete, functionality available in other modes. Also not working properly. Returning 1 value instead of 3
    %             * NXTLight-Sensor:
    %                 * DeviceMode.NXTLight.Reflect *[Default]*
    %		          * Output: value in range 0% to 100% brightness
    %                 * DeviceMode.NXTLight.Ambient
    %		          * Output: value in range 0% to 100% brightness
    %             * NXTSound-Sensor:
    %                 * DeviceMode.NXTSound.DB *[Default]*
    %		          * Output: value in decibel
    %                 * DeviceMode.NXTSound.DBA
    %		          * Output: value in dba weighted according to human hearing
    %             * NXTTemperature-Sensor
    %                 * DeviceMode.NXTTemperature.C *[Default]*
    %		          * Output: value in Celsius
    %                 * DeviceMode.NXTTemperature.F
    %		          * Output: value in Fahrenheit
    %             * NXTTouch-Sensor:
    %                 * DeviceMode.NXTTouch.Pushed *[Default]*
    %		          * Output: 0: not pushed, 1: pushed
    %                 * DeviceMode.NXTTouch.Bumps
    %		          * Output: n: number of times pressed and released
    %             * NXTUltraSonic-Sensor:
    %                 * DeviceMode.NXTUltraSonic.CM *[Default]*
    %		          * Output: distance in cm
    %                 * DeviceMode.NXTUltraSonic.IN
    %		          * Output: distance in inches
    %             * HTAccelerometer-Sensor:
    %                 * DeviceMode.HTAccelerometer.Acceleration *[Default]*
    %                 * DeviceMode.HTAccelerometer.AccelerationAllAxes
    %		          * Note: Not working properly. Returning 1 value instead of 6
    %             * HTCompass-Sensor:
    %                 * DeviceMode.HTCompass.Degrees *[Default]*
    %		          * Note: 'Error' mode assigned, value still appears to be correct.
    %		          * Output: 0 to 180 degree. 45° being north, 90° east etc
    %             * HTColor-Sensor:
    %                 * DeviceMode.HTColor.Col *[Default]*
    %		          * Output: value representing color: 0-black, 1-purple, 2-blue, 3-cyan, 4-green, 5-green/ yellow, 6-yellow, 7-orange, 8-red, 9-magenta, 10-pink, 11-low saturation blue, 12-low saturation green, 13-low saturation yellow, 14-low saturation orange, 15-low saturation red, 16-low saturation pink, 17-white
    %                 * DeviceMode.HTColor.Red
    %		          * Output: value in range 0 to 255 of red reflectivity
    %                 * DeviceMode.HTColor.Green
    %		          * Output: value in range 0 to 255 of green reflectivity
    %                 * DeviceMode.HTColor.Blue
    %		          * Output: value in range 0 to 255 of blue reflectivity
    %                 * DeviceMode.HTColor.White
    %		          * Output: value in range 0 to 255 of white reflectivity
    %                 * DeviceMode.HTColor.Raw
    %		          * Note: obsolete, color values available in other modes. Also not working properly. Returning 1 value instead of 3
    %                 * DeviceMode.HTColor.Nrm,
    %		          * Note: obsolete, normalized values available in other modes. Also not working properly. Returning 1 value instead of 4
    %                 * DeviceMode.HTColor.All
    %		          * Note: obsolete, all values available in other modes. Also not working properly. Returning 1 value instead of 4
    %    debug (bool): Debug turned on or off. In debug mode, everytime a command is passed to 
    %        the sublayer ('communication layer'), there is feedback in the console about what 
    %        command has been called. *[WRITABLE]*
    %    value (numeric): Value read from hysical sensor. What the value represents depends on
    %        :attr:`mode`. *[READ-ONLY]*
    %    type (DeviceType): Type of physical sensor connected to the port. Possible types are: [READ-ONLY]
    %
    %             * DeviceType.NXTTouch
    %             * DeviceType.NXTLight
    %             * DeviceType.NXTSound
    %             * DeviceType.NXTColor
    %             * DeviceType.NXTUltraSonic
    %             * DeviceType.NXTTemperature
    %             * DeviceType.LargeMotor
    %             * DeviceType.MediumMotor
    %             * DeviceType.Touch 
    %             * DeviceType.Color 
    %             * DeviceType.UltraSonic 
    %             * DeviceType.Gyro 
    %             * DeviceType.InfraRed 
    %             * DeviceType.HTColor
    %             * DeviceType.HTCompass
    %             * DeviceType.HTAccelerometer
    %             * DeviceType.Unknown
    %             * DeviceType.None 
    %             * DeviceType.Error
    %

    
    properties  % Standard properties to be set by user
        % mode (DeviceMode.{Type}): Sensor mode in which the value will be read. By default, 
        %     mode is set to DeviceMode.Default.Undefined. Once a physical sensor is connected
        %     to the port *and* the physical Brick is connected to the EV3-object afterwards, the allowed 
        %     mode and the default mode for a Sensor-object are the following (depending on the
        %     sensor type): [WRITABLE]
        %
        %     * Touch-Sensor: 
        %         * DeviceMode.Touch.Pushed [Default]
        %         * DeviceMode.Touch.Bumps
        %     * Ultrasonic-Sensor: 
        %         * DeviceMode.UltraSonic.DistCM [Default]
        %         * DeviceMode.UltraSonic.DistIn
        %         * DeviceMode.UltraSonic.Listen
        %     * Color-Sensor: 
        %         * DeviceMode.Color.Reflect [Default]
        %         * DeviceMode.Color.Ambient
        %         * DeviceMode.Color.Col
        %     * Gyro-Sensor: 
        %         * DeviceMode.Gyro.Angular [Default]
        %         * DeviceMode.Gyro.Rate
        %     * Infrared-Sensor:
        %         * DeviceMode.InfraRed.Prox [Default]
        %         * DeviceMode.InfraRed.Seek
        %         * DeviceMode.InfraRed.Remote
        %     * NXTColor-Sensor:
        %         * DeviceMode.NXTColor.Reflect [Default]
        %         * DeviceMode.NXTColor.Ambient
        %         * DeviceMode.NXTColor.Color
        %         * DeviceMode.NXTColor.Green
        %         * DeviceMode.NXTColor.Blue
        %         * DeviceMode.NXTColor.Raw
        %     * NXTLight-Sensor:
        %         * DeviceMode.NXTLight.Reflect [Default]
        %         * DeviceMode.NXTLigth.Ambient
        %     * NXTSound-Sensor:
        %         * DeviceMode.NXTSound.DB [Default]
        %         * DeviceMode.NXTSound.DBA
        %     * NXTTemperature-Sensor
        %         * DeviceMode.NXTTemperature.C [Default]
        %         * DeviceMode.NXTTemperature.F
        %     * NXTTouch-Sensor:
        %         * DeviceMode.NXTTouch.Pushed [Default]
        %         * DeviceMode.NXTTouch.Bumps
        %     * NXTUltraSonic-Sensor:
        %         * DeviceMode.NXTUltraSonic.CM [Default]
        %         * DeviceMode.NXTUltraSonic.IN
        %     * HTAccelerometer-Sensor:
        %         * DeviceMode.HTAccelerometer.Acceleration [Default]
        %         * DeviceMode.HTAccelerometer.AccelerationAllAxes
        %     * HTCompass-Sensor:
        %         * DeviceMode.HTCompass.Degrees [Default]
        %     * HTColor-Sensor:
        %         * DeviceMode.HTColor.Col [Default]
        %         * DeviceMode.HTColor.Red
        %         * DeviceMode.HTColor.Green
        %         * DeviceMode.HTColor.Blue
        %         * DeviceMode.HTColor.White
        %         * DeviceMode.HTColor.Raw
        %         * DeviceMode.HTColor.Nr,
        %         * DeviceMode.HTColor.All
        % See also SENSOR.VALUE, SENSOR.TYPE 
        mode;
        
        % debug (bool): Debug turned on or off. In debug mode, everytime a command is passed to 
        %     the sublayer ('communication layer'), there is feedback in the console about what 
        %     command has been called. [WRITABLE]
        debug;
    end
    
    properties (Dependent)  % Parameters to be read directly from physical brick
        % value (numeric): Value read from physical sensor. [READ-ONLY]
        %     What the value represents depends on sensor.mode. 
        % See also SENSOR.MODE
        value;
        
        % type (DeviceType): Type of physical sensor connected to the port. [READ-ONLY]
        %     Possible types are:
        %     * DeviceType.NXTTouch
        %     * DeviceType.NXTLight
        %     * DeviceType.NXTSound
        %     * DeviceType.NXTColor
        %     * DeviceType.NXTUltraSonic
        %     * DeviceType.NXTTemperature
        %     * DeviceType.LargeMotor
        %     * DeviceType.MediumMotor
        %     * DeviceType.Touch 
        %     * DeviceType.Color 
        %     * DeviceType.UltraSonic 
        %     * DeviceType.Gyro 
        %     * DeviceType.InfraRed 
        %     * DeviceType.HTColor
        %     * DeviceType.HTCompass
        %     * DeviceType.HTAccelerometer
        %     * DeviceType.Unknown
        %     * DeviceType.None 
        %     * DeviceType.Error
        type; 
    end

    properties (Hidden, Access = private)  % Hidden properties for internal use only 
        % ev3Handle (EV3): Handle to the EV3-object to which this sensor instance belongs
        ev3Handle;
        
        % port (string): Sensor port. 
        %     This is only the string representation of the sensor port to work with.
        %     Internally, SensorPort-enums are used.
        port; 
        
        % init (bool): Indicates init-phase (i.e. constructor is running).
        init = true;
    end   
    
    properties (Hidden, Dependent, Access = private)
        %physicalSensorConnected (bool): True if physical sensor is connected to this port
        physicalSensorConnected;
    end
    
    methods  % Standard methods
        %% Constructor
        function sensor = Sensor(varargin)
            % Sets properties of Sensor-object and indicates end of init-phase when it's done
            %
            % Notes:
            %     * input-arguments will directly be handed to Motor.setProperties
            %
            % Arguments:
            %     varargin: see setProperties(sensor, varargin)
            %
            
            sensor.setProperties(varargin{1:end});
			sensor.init = false;
        end
        
        %% Brick functions
        function reset(sensor)
            % Resets sensor value.
            %
            % Notes:
            %     * Has not been thoroughly tested but seems to work as expected
            %
            
            sensor.handleCommand(@inputDeviceClrChanges, false, 0, sensor.port);
        end
        
        %% Setter
        function set.mode(sensor, mode)
            if strcmp(class(mode),'DeviceMode.Default') && ~sensor.physicalSensorConnected
                sensor.mode = mode;
                return;
            end
            
            type = sensor.type;
            if ~isModeValid(mode, type)
                error('Invalid sensor mode.');
            end
            
            sensor.mode = mode;
            if ~strcmp(class(mode),'DeviceMode.Default') && sensor.ev3Handle.isConnected 
                try
                    sensor.setMode(mode);  % Update physical brick's mode parameter
                catch
                    % Ignore
                end
            end
        end
        
        function set.debug(sensor, debug)
            % Check if debug is valid and set sensor.debug if it is.
            if ~isBool(debug)
                error('Given parameter is not a bool.');
            end
            
            sensor.debug = str2bool(debug);
        end
        
        function set.port(sensor, port)
            if ~isPortStrValid(class(sensor),port)
                error('Given port is not a valid port.');
            end
            
            sensor.port = str2PortParam(class(sensor), port);
        end
        
        function setProperties(sensor, varargin)
            % Sets multiple Sensor properties at once using MATLAB's inputParser.
            %
            % Arguments:
            %     debug (bool): *[OPTIONAL]*
            %     mode (DeviceMode.{Type}): *[OPTIONAL]*
            %
            % ::
            %
            %     Example:
            %         brick = EV3()
            %         brick.connect('bt', 'serPort', '/dev/rfcomm0');
	    %     
	    %         % use the following line:
            %         brick.sensor1.setProperties('debug', 'on', 'mode', DeviceMode.Color.Ambient);
            %     
	    %         % Instead of: 
	    %         brick.sensor1.debug = 'on';
            %         brick.sensor1.mode = DeviceMode.Color.Ambient;
            %
            p = inputParser();
            
            % Set default values
            if sensor.init
                defaultDebug = 0;
                defaultMode = DeviceMode.Default.Undefined;
            else
                defaultDebug = sensor.debug;
                defaultMode = sensor.mode;
            end
            
            % Add parameter
            if sensor.init
                p.addRequired('port');
                p.addRequired('ev3Handle');
            end
            p.addOptional('debug', defaultDebug);
            p.addOptional('mode', defaultMode);
            
            % Parse...
            p.parse(varargin{:});
            
            % Set properties
            if sensor.init
                sensor.port = p.Results.port;
                sensor.ev3Handle = p.Results.ev3Handle;
            end
            sensor.mode = p.Results.mode;
            sensor.debug = p.Results.debug;
        end
        
        %% Getter
        function value = get.value(sensor)
            value = 0;
            
            if sensor.ev3Handle.isConnected
                value = sensor.getValue();
                if isnan(value)
                    warning('Could not detect sensor at port %d.', ...
                        sensor.port+1);
                    value = 0;
                end
            end
        end
        
        function conn = get.physicalSensorConnected(sensor)
            currentType = sensor.type;
            conn = (currentType<DeviceType.Unknown && ... 
                (currentType~=DeviceType.MediumMotor && currentType~=DeviceType.LargeMotor));
        end
        
        function sensorType = get.type(sensor)
            if sensor.ev3Handle.isConnected
                [sensorType, ~] = sensor.getTypeMode(); 
            else
                sensorType = DeviceType.Unknown;
            end
        end
        
        %% Display
        function display(sensor)
            displayProperties(sensor); 
        end
    end
    
    methods (Access = private)  % Private brick functions that are wrapped by dependent params
        function setMode(sensor, mode)
            sensor.handleCommand(@inputReadSI, true, 0, sensor.port, mode);  % Reading a value implicitly sets the mode.
        end
        
        function val = getValue(sensor)
            %getValue Reads value from sensor
            % 
            % Notes:
            %  * After changing the mode, sensors initially always send an invalid value. In
            %    this case, the inputReadSI-opCode is sent again to get the correct value.
            %
            val = sensor.handleCommand(@inputReadSI, false, 0, sensor.port, sensor.mode);
            
            if strcmp(class(sensor.mode), 'DeviceMode.Color')
                if sensor.mode == DeviceMode.Color.Col
                    val = Color(val);
                end
            end
            
            % See note
			if isnan(val)
				val = sensor.handleCommand(@inputReadSI, false, 0, sensor.port, sensor.mode);
            end
        end
        
        function status = getStatus(sensor)
           statusNo = sensor.handleCommand(@inputDeviceGetConnection, false, 0, sensor.port);
           status = ConnectionType(statusNo);
        end
        
        function [type,mode] = getTypeMode(sensor)
            type = DeviceType.Error;
            
            % In very rare cases, the brick sends an invalid type-no - try to get a valid one up
            % to ten times (I know, I know, another ugly workaround :( )
            for i = 1:10
                try
                    [typeNo,modeNo] = sensor.handleCommand(@inputDeviceGetTypeMode, false, 0, sensor.port);
                    type = DeviceType(typeNo);
                catch ME
                    continue;
                end
                
                break;
            end
                
            try
                mode = DeviceMode(type,modeNo);
            catch ME
                mode = DeviceMode.Default.Undefined;
            end
        end
        
        function varargout = handleCommand(sensor, command, checkDevice, varargin)
            % Execute a CommunicationInterface-method given as a handle
            %
            % As those methods have different, fixed numbers of output arguments, this quantity
            % has to be retrieved first.
            
            if ~sensor.ev3Handle.isConnected
                msg = ['Not connected to physical brick. ',...
                       'You have to call connect(...) on the EV3 object first!'];
                id = [ID(), ':', 'NotConnected'];
                
                throw(MException(id, msg));
            elseif checkDevice && ~sensor.physicalSensorConnected
                msg = ['Could not detect sensor at port %d', sensor.port+1, '.'];
                id = [ID(), ':', 'NoDevice'];
                
                throw(MException(id, msg));
            end
            
            if sensor.debug
                fprintf('(DEBUG) Sending %s\n', func2str(command));
            end
            
            % Note: Arrg. MATLAB does not support nargout for class methods directly, so I have to
            % do this ugly workaround using strings. See 
            % https://de.mathworks.com/matlabcentral/answers/96617-how-can-i-use-nargin-nargout-to-determine-the-number-of-input-output-arguments-of-an-object-method
            nOut = nargout(strcat('CommunicationInterface>CommunicationInterface.', func2str(command)));
            [varargout{1:nOut}] = sensor.ev3Handle.dispatch(command, nOut, varargin{:});
        end 
    end
    
    methods (Access = ?EV3)
        function resetPhysicalSensor(sensor)
            % Reset mode to default and reset sensor value
            %
            % Notes: 
            %     * This is called automatically on disconnect
            
            if ~sensor.ev3Handle.isConnected || ~sensor.physicalSensorConnected
                return
            end
            
            try
                sensor.mode = DeviceMode(sensor.type, uint8(0));
                sensor.reset;
            catch ME
                % For now: ignore...
            end
        end
    end
end