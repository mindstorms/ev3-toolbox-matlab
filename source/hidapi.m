classdef hidapi < handle      
    % Interface to the hidapi library
    % 
    % Notes:
    %     * Developed from the hidapi available at http://www.signal11.us/oss/hidapi/.
    %     * Windows: hidapi.dll needed.
    %     * Mac: hidapi.dylib needed. In addition, Xcode has to be installed.
    %     * Linux: hidapi has to be compiled on host-system. 
    % 
    % Attributes:
    %     handle ():
    %     vendorID (numeric): Vendor-ID of the USB device.
    %     productID (numeric): Product-ID of the USB device. 
    %     nReadBuffer (numeric): Read-buffer size in bytes.
    %     nWriteBuffer (numeric): Write-buffer size in bytes. Needs to be 1 Byte bigger than
    %        actual packet.
    %     slib (string): Name of shared library file (without file extension). Defaults to
    %        'hidapi'.
    %     sheader (string): Name of shared library header. Defaults to 'hidapi.h'.
    %
    % Example:
    %     hidHandle = hidapi(1684,0005,1024,1025); %|br|
    %
    
    properties
        % handle: Connection handle to the device.
        handle
        % vendorID (numeric): Vendor-ID of the USB device.
        vendorID;
        % productID (numeric): Product-ID of the USB device.
        productID;
        % nReadBuffer (numeric): Read-buffer size in bytes. 
        nReadBuffer;
        % nWriteBuffer (numeric): Write-buffer size in bytes.
        %    Needs to be 1 Byte bigger than actual packet.
        nWriteBuffer;
        % slib (string): Name of shared library file (without file extension). Defaults to 'hidapi'.
        slib = 'hidapi';
        % sheader (string): Name of shared library header. Defaults to 'hidapi.h'.
        sheader = 'hidapi.h';
    end
    
    methods
        %% Constructor
        
        function hid = hidapi(vendorID,productID,nReadBuffer,nWriteBuffer)
            % Create a hidapi library interface object from the corresponding library 
            %
            % Arguments:
            %     vendorID (numeric): Vendor-ID of the USB device in decimal.
            %     productID (numeric): Product-ID of the USB device in decimal. 
            %     nReadBuffer (numeric): Read-buffer size in bytes.
            %     nWriteBuffer (numeric): Write-buffer size in bytes. Needs to be 1 Byte bigger than
            %        actual packet.
            %
            % Raises:
            %     LoadingLibraryError: Could not load .dll/.dylib/.so-file of hidapi.
            %     InvalidFileNameOrFileMissing: Either file names given were wrong or the files 
            %        are missing (thunk files, proto files, ...).
            %
            
            hid.vendorID = vendorID;
            hid.productID = productID;
            hid.nReadBuffer = nReadBuffer;
            hid.nWriteBuffer = nWriteBuffer;
            
            % Disable warnings
            warning('off','MATLAB:loadlibrary:TypeNotFoundForStructure');
            warning('off', 'MATLAB:loadlibrary:ClassIsLoaded');
            
            try
                % Check the operating system type and load slib
                if (ispc == 1)
                    % Check the bit version
                    if (strcmp(mexext,'mexw32'))
                        hid.slib = 'hidapi32';
                        % Load the library via the proto file
                        loadlibrary(hid.slib,@hidapi32_proto,'alias','hidapiusb')
                    elseif (strcmp(mexext,'mexw64'))
                        hid.slib = 'hidapi64';
                        % Load the library via the proto file
                        loadlibrary(hid.slib,@hidapi64_proto,'alias','hidapiusb')
                    end
                elseif (ismac == 1)
                    hid.slib = 'hidapi64';
                    % Load the library via the proto file
                    loadlibrary(hid.slib,@hidapi64mac_proto,'alias','hidapiusb');
                elseif (isunix == 1)
                    hid.slib = 'libhidapi-libusb';
                    % Load the shared library
                    loadlibrary(hid.slib,@hidapi_libusb_proto,'alias','hidapiusb');
                end
            catch ME
                % Create own exception for clarification
                id = [hid.slib, ':', 'LoadingLibraryError'];
                msg = strcat({'Could not load library '}, {hid.slib}, {'.'});
                exception = MException(id, msg);

                % Try to narrow down loading failure
                if isempty(findstr(ME.identifier, 'LoadFailed')) ...
                        && isempty(findstr(ME.identifier, 'ErrorLoadingLibrary')) ...
                        && isempty(findstr(ME.identifier, 'ErrorInHeader'))
                    id = [hid.slib, ':', 'InvalidFileNameOrFileMissing'];
                    msg = 'Invalid file names were given or files are not available.';
                    cause = MException(id, msg);
                    exception = addCause(exception, cause);
                end

                throw(exception);
            end
            
            % Remove the library extension
            hid.slib = 'hidapiusb';

%             if hid.debug > 0
%                 libfunctionsview('hidapiusb');
%             end
        end
        
        function delete(hid)
            % Close an open hid device connection and delete the object. 
            %
            % Notes:
            %     * You cannot unloadlibrary in this function as the object is still present 
            %       in the MATLAB work space. Todo: Any alternative?
        end
        
        %% HIDAPI-Functions
        
        function open(hid)
            % Open a connection with a hid device
            %
            % Throws:
            %     CommError: Error during communication with device
            %
            % Notes: 
            %     * Gets called automatically when creating an hidapi-object.
            %     * The pointer return value from this library call is always null so it is not 
            %       possible to know if the open was successful.
            %     * The final parameter to the open hidapi library call has different types 
            %       depending on OS. On windows it is uint16, on linux/mac int32.
            
            % Create a null pointer for the hid_open function (depends on OS)
            if (ispc == 1)
                pNull = libpointer('uint16Ptr');
            elseif ((ismac == 1) || (isunix == 1))
                pNull = libpointer('int32Ptr');
            end
            
            % Open the hid interface
            [newHandle,value] = calllib(hid.slib,'hid_open',uint16(hid.vendorID), ...
                uint16(hid.productID),pNull);
            
            % (MMI) Assert error case (hid_open returns null-pointer in error case)
            assert(isLibPointerValid(newHandle)==1, ...
                [hid.slib, ':', 'CommError'], ...
                'Failed to connect to USB device.');
            
            hid.handle = newHandle;
        end
        
        function close(hid)
            % Close the connection to a hid device. 
            %
            % Throws:
            %     InvalidHandle: Handle to USB-device not valid
            %
            % Notes:
            %     * Gets called automatically when deleting the hidapi instance.
            
            % (MMI) Check if pointer is (unexpectedly) already invalidated
            assert(isLibPointerValid(hid.handle)==1, ...
                [hid.slib, ':', 'InvalidHandle'], ...
                'Failed to close USB-connection because pointer to USB-device is already invalidated.');
            
            % Close the connection
            calllib(hid.slib,'hid_close',hid.handle);
            
            % Invalidate the pointer
            hid.handle = [];
        end
        
        % Implemented @ MMI
        function rmsg = read_timeout(hid, timeOut)
            % Read from a hid device with a timeout and return the read bytes. 
            %
            % Arguments:
            %     timeOut (numeric >= 0): Milliseconds after which a timeout-error occurs if no 
            %         packet could be read.
            %
            % Throws:
            %     CommError: Error during communication with device
            %     InvalidHandle: Handle to USB-device not valid
            %
            
            % Read buffer of nReadBuffer length
            buffer = zeros(1,hid.nReadBuffer);
            % Create a uint8 pointer
            pbuffer = libpointer('uint8Ptr', uint8(buffer));
            
            % Check if pointer is (unexpectedly) already invalidated
            assert(isLibPointerValid(hid.handle)==1, ...
                [hid.slib, ':', 'InvalidHandle'], ...
                'Failed to read USB-data because pointer to USB-device is invalidated.');
            
            % Read data from HID device
            [res,h] = calllib(hid.slib,'hid_read_timeout',hid.handle,pbuffer,uint64(length(buffer)),timeOut);
            
            % Check the response (No assert as there are multiple cases)
            if res < 1
                % Error occurred
                id = [hid.slib, ':', 'CommError'];
                % Narrow error down
                if res == -1
                    msg = 'Connection error (probably lost connection to device)';
                elseif res == 0
                    msg = ['Could not read data from device (device is still connected, ',...
                           'but does not react)'];
                else
                    msg = 'Unexpected connection error';
                end
                causeException = MException(id, msg);
                ME = MException(id, 'Failed to read data via USB.');
                addCause(ME, causeException);
                throw(ME);
            end
            
            % Return the string value
            rmsg = pbuffer.Value;
        end
        
        function rmsg = read(hid)
            % Read from a hid device and returns the read bytes.
            %
            % Throws:
            %     CommError: Error during communication with device
            %     InvalidHandle: Handle to USB-device not valid
            % 
            % Notes: 
            %     * Will print an error if no data was read.
            %
            
            % Read buffer of nReadBuffer length
            buffer = zeros(1,hid.nReadBuffer);
            % Create a uint8 pointer
            pbuffer = libpointer('uint8Ptr', uint8(buffer));
            
            % (MMI) Check if pointer is (unexpectedly) already invalidated
            assert(isLibPointerValid(hid.handle)==1, ...
                [hid.slib, ':', 'InvalidHandle'], ...
                'Failed to read USB-data because pointer to USB-device is invalidated.');
            
            % Read data from HID device
            [res,h] = calllib(hid.slib,'hid_read',hid.handle,pbuffer,uint64(length(buffer)));
            
            % (MMI) Check the response (No assert as there are multiple cases)
            if res < 1
                % Error occurred
                id = [hid.slib, ':', 'CommError'];
                % Narrow error down
                if res == -1
                    msg = 'Connection error (probably lost connection to device)';
                elseif res == 0
                    msg = ['Could not read data from device (device is still connected, ',...
                           'but does not react)'];
                else
                    msg = 'Unexpected connection error';
                end
                causeException = MException(id, msg);
                ME = MException(id, 'Failed to read data via USB.');
                addCause(ME, causeException);
                throw(ME);
            end
            
            % Return the string value
            rmsg = pbuffer.Value;
        end
        
        function write(hid,wmsg,reportID)
            % Write to a hid device.
            %
            % Throws:
            %     CommError: Error during communication with device
            %     InvalidHandle: Handle to USB-device not valid
            %
            % Notes: 
            %     * Will print an error if there is a mismatch between the buffer size and the 
            %       reported number of bytes written.
            %
            
            % Append a 0 at the front for HID report ID
            wmsg = [reportID wmsg];
            
            % Pad with zeros for nWriteBuffer length
            % (MMI) Note:: The following line does not seem to be necessary;
            % wmsg does not need to be the max packet size. Uncommenting this doesn't affect
            % anything, and I would prefer sending short messages over long ones.
            % Further testing may be required, so for now I don't change a thing.
            % (MMI) Update:: Okay, so under Windows, this line IS necessary, as well as the
            % fixed write-buffer size of 1025 bytes for EV3 (== wMaxPacketSize+1; smaller packets fail; 
            % bigger packets do get handled by the brick, but the second assertion below will fail).
            wmsg(end+(hid.nWriteBuffer-length(wmsg))) = 0;
            
            % Create a uint8 pointer
            pbuffer = libpointer('uint8Ptr', uint8(wmsg));
            
            % (MMI) Check if pointer is (unexpectedly) already invalidated
            assert(isLibPointerValid(hid.handle)==1, ...
                [hid.slib, ':', 'InvalidHandle'], ...
                'Failed to write to USB because pointer to USB-device is invalidated.');
            
            % Write the message
            [res,h] = calllib(hid.slib,'hid_write',hid.handle,pbuffer,uint64(length(wmsg)));
            
            % (MMI) Check the response
            assert(res == length(wmsg), ...
                [hid.slib, ':', 'CommError'], ...
                'Failed to write data via USB.');
        end
        
        function str = getHIDInfoString(hid,info)
            % Get the corresponding hid info from the hid device.
            %
            % Throws:
            %     CommError: Error during communication with device
            %     InvalidHandle: Handle to USB-device not valid
            %
            % Notes:
            %     * Info is the hid information string. 
            %
            % See also HIDAPI.GETMANUFACTURERSSTRING, HIDAPI.GETPRODUCTSTRING,
            % HIDAPI.GETSERIALNUMBERSTRING.
            %
            
            % Read buffer nReadBuffer length
            buffer = zeros(1,hid.nReadBuffer);
            % Create a libpointer (depends on OS)
            if (ispc == 1)
                pbuffer = libpointer('uint16Ptr', uint16(buffer));
            elseif ((ismac == 1) || (isunix == 1))
                pbuffer = libpointer('int32Ptr', uint32(buffer));
            end
            
            % (MMI) Check if pointer is (unexpectedly) already invalidated
            assert(isLibPointerValid(hid.handle)==1, ...
                [hid.slib, ':', 'InvalidHandle'], ...
                'Failed to read USB-data because pointer to USB-device is invalidated.');
            
            % Get the HID info string
            [res,h] = calllib(hid.slib,info,hid.handle,pbuffer,uint32(length(buffer)));
            
            % (MMI) Check the response
            assert(res~=-1, ...
                [hid.slib, ':', 'CommError'], ...
                'Failed to read HID info string.');
            
            % Return the string value
            str = sprintf('%s',char(pbuffer.Value));
        end
        
        function setNonBlocking(hid,nonblock)
            % Set the non blocking flag on the hid device connection.
            %
            % Arguments:
            %     nonblock (numeric in {0,1}): 0 disables nonblocking, 1 enables nonblocking
            %
            % Throws:
            %     CommError: Error during communication with device
            %     InvalidHandle: Handle to USB-device not valid
            %
            
            % (MMI) Check if pointer is (unexpectedly) already invalidated
            assert(isLibPointerValid(hid.handle)==1, ...
                [hid.slib, ':', 'InvalidHandle'], ...
                'Failed to set USB-read-mode to non-blocking because pointer to USB-device is invalidated.');
            
            % Set non blocking
            [res,h] = calllib(hid.slib,'hid_set_nonblocking',hid.handle,uint32(nonblock));
            
            % (MMI) Check the response
            assert(res~=-1, ...
                [hid.slib, ':', 'CommError'], ...
                'Failed to set USB-read-mode to non-blocking.');
        end
        
        function init(hid)
            % Inits the hidapi library. 
            %
            % Throws:
            %     CommError: Error during communication with device
            %
            % Notes:
            %     * This is called automatically in the library itself with the open function. You 
            %       should not have to call this function directly.
            %
            
            warning([hid.slib, ':', 'RedundantCall'], ...
                'The init-function gets called automatically when connecting!');
            
            % Init device
            res = calllib(hid.slib,'hid_init');
            
            % (MMI) Check the response
            assert(res~=-1, ...
                [hid.slib, ':', 'CommError'], ...
                'Failed to init USB-device.');
        end
        
        function exit(hid)
            %hidapi.exit Exit hidapi
            %
            % hid.exit() exits the hidapi library.
            %
            % Throws:
            %     CommError: Error during communication with device
            %
            % Notes::
            % - You should not have to call this function directly.
            
            warning([hid.slib, ':', 'RedundantCall'], ...
                'The exit-function gets called automatically when disconnecting!');
            
            % Exit device
            res = calllib(hid.slib,'hid_exit');
            
            % (MMI) Check the response
            assert(res~=-1, ...
                [hid.slib, ':', 'CommError'], ...
                'Failed to exit USB-device.');
        end
        
        function str = error(hid)
            % Return the hid device error string if a function produced an error.
            %
            % Throws:
            %     InvalidHandle: Handle to USB-device not valid
            %
            % Notes:
            %     * This function must be called explicitly if you think an error was generated 
            %       from the hid device.
            % 
            
            % (MMI) Check if pointer is (unexpectedly) already invalidated
            assert(isLibPointerValid(hid.handle)==1, ...
                [hid.slib, ':', 'InvalidHandle'], ...
                'Failed to read USB-error-data because pointer to USB-device is invalidated.');
            
            [~,str] = calllib(hid.slib,'hid_error',hid.handle);
        end
        
        function str = enumerate(hid,vendorID,productID)
            % Enumerates the info about the hid device with the given vendorID and productID 
            % and returns a string with the returned hid information.
            %
            % Arguments:
            %     vendorID (numeric): Vendor-ID of the USB device in decimal.
            %     productID (numeric): Product-ID of the USB device in decimal. 
            %
            % Notes:
            %     * Using a vendorID and productID of (0,0) will enumerate all connected hid 
            %       devices.
            %     * MATLAB does not have the hid_device_infoPtr struct so some of the returned 
            %       information will need to be resized and cast into uint8 or chars.
            %
            
            % Enumerate the hid devices
            str = calllib(u.slib,'hid_enumerate',uint16(vendorID),uint16(productID));
        end
        
        %% Wrapper 
        
        function str = getManufacturersString(hid)
            % Get manufacturers string from hid object using getHIDInfoString.
            str = getHIDInfoString(hid,'hid_get_manufacturer_string');
        end
        function str = getProductString(hid)
            % Get product string from hid object using getProductString.
            str = getHIDInfoString(hid,'hid_get_product_string');
        end
        function str = getSerialNumberString(hid)
            % Get serial number from hid object using getSerialNumberString.
            str = getHIDInfoString(hid,'hid_get_serial_number_string');
        end
    end
end

% Implemented @ MMI
function valid = isLibPointerValid(handle)
    % Check whether hid.handle is valid libpointer

    valid = 0;
    if ~isempty(handle)
        if isa(handle, 'handle') && ~isNull(handle)
            valid = 1;
        end
    end
end
